from datetime import date
from models import Person, initial_DB


def test_db():
    initial_DB()
    uncle_bob = Person(first_name='Lucas', last_name='Casco', birthday=date(1988, 12, 13))
    uncle_bob.save()

    grandma = Person.create(first_name='Lucas', last_name='Casco', birthday=date(1988, 12, 13))

    print(grandma.first_name)

if __name__ == '__main__':
    test_db()
