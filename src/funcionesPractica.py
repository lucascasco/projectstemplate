def make_fibonacci (limit, verbose) -> list:
    """  Genera la serie de Fibonacci hasta n.  
    Parametro:
       limit: hasta el numero que se debe generar.
       verbose: Si debe imprimir la serie  """
    
    a = 0
    b = 1
    
    result = []
    
    while a < limit:
        if verbose:
            print (a, end = ', ')
        result.append (a)
        a, b = b ,a + b 
    if verbose:
        print()
    return result 

make_fibonacci(200, True)


            